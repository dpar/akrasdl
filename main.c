#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

SDL_Renderer *renderer;
SDL_Window *window;
SDL_TimerID current_timer = 0;
enum status {
	LOCKED,
	POMO,
	ADJUST,
	SHORTCUTS,
};

const int POMOLEN = 25 * 60 * 1000;
const int ADJLEN = 30 * 1000;

enum status status = LOCKED;

int drawui() {
	switch(status) {
	case LOCKED:
		SDL_SetRenderDrawColor(renderer, 0, 64, 64, 255);
		break;
	case POMO:
		SDL_SetRenderDrawColor(renderer, 128, 64, 64, 255);
		break;
	case ADJUST:
		SDL_SetRenderDrawColor(renderer, 64, 64, 128, 255);
		break;
	case SHORTCUTS:
		SDL_SetRenderDrawColor(renderer, 0, 64, 128, 255);
		break;
	}
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
}

Uint32 timer_end(Uint32 interval, void *param) {
	system("notify-send end -t 3000");
	return 6000;
}

int main(int argc, char *argv[]) {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
		fprintf(stderr, "Could not initialize SDL: %s\n", SDL_GetError());
		return 1;
	}
	SDL_SetHint(SDL_HINT_GRAB_KEYBOARD, "1");

	window = SDL_CreateWindow("Akrasdl",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640, 480,
		SDL_WINDOW_RESIZABLE |
		SDL_WINDOW_INPUT_GRABBED);
	if (!window) {
		fprintf(stderr, "Could not create window: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}
	renderer = SDL_CreateRenderer(window, -1, 0);
	if (!renderer) {
		fprintf(stderr, "Could not create renderer: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}
	drawui();
	SDL_Event event;
	while (SDL_WaitEvent(&event)) {
		if (event.type == SDL_QUIT) {
			break;
		}
		if (event.type == SDL_KEYDOWN) {
			SDL_Keycode sym = event.key.keysym.sym;
			switch(status) {
			case POMO:
				if (sym == SDLK_e) {
					if (current_timer > 0) {
						SDL_RemoveTimer(current_timer);
					}
					system("akrasdl-run pomo-end");
					SDL_SetWindowGrab(window, SDL_TRUE);
					status = LOCKED;
				}
				break;
			case ADJUST:
				if (sym == SDLK_e) {
					if (current_timer > 0) {
						SDL_RemoveTimer(current_timer);
					}
					SDL_SetWindowGrab(window, SDL_TRUE);
					status = LOCKED;
				}
				break;
			case SHORTCUTS:
				if (SDL_isalnum(sym)) {
					char buf[64];
					snprintf(buf, sizeof(buf), "akrasdl-run shortcut %c", (char) sym);
					system(buf);
				}
				status = LOCKED;
				break;
			case LOCKED:
				switch (sym) {
				case SDLK_p:
					if (system("akrasdl-run pomo-start") == 0) {
						current_timer = SDL_AddTimer(POMOLEN, timer_end, NULL);
						status = POMO;
						SDL_SetWindowGrab(window, SDL_FALSE);
					}
					break;
				case SDLK_a:
					current_timer = SDL_AddTimer(ADJLEN, timer_end, NULL);
					status = ADJUST;
					SDL_SetWindowGrab(window, SDL_FALSE);
					break;
				case SDLK_SPACE:
					status = SHORTCUTS;
					break;
				}
				break;
			}
		}
		drawui();
	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
