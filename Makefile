CFLAGS=`sdl2-config --cflags -Wall -Wextra -Werror`
LIBS=`sdl2-config --libs`

akrasdl: main.c
	$(CC) $(CFLAGS) $< -o $@ $(LIBS)
